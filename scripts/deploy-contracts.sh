#!/usr/bin/env bash
HOME=$(pwd)
cd ../contracts/
truffle deploy
npx yarn run clean-contracts
cd $HOME
./scripts/import-contracts.sh
